# encoding: UTF-8

import utilities
import json

INDEXES_FILENAME = 'activity_net.v1-3.min.json'
INDEXES_URL = 'http://ec2-52-11-11-89.us-west-2.compute.amazonaws.com/files/activity_net.v1-3.min.json'
INDEXES_MD5 = 'd4579f422904650b7be4d3178abb4294'


def activity_net_index_walker(index: dict):
    database = index['database']
    for vid, info in database.items():
        subset = info['subset']
        yield vid, subset


def main():
    m = utilities.PathMapper('activity-net')
    print('Fetching {!r}'.format(INDEXES_FILENAME))
    utilities.fetch(mapper=m, relpath=INDEXES_FILENAME, url=INDEXES_URL, md5=INDEXES_MD5)
    with open(m.resolve(INDEXES_FILENAME)) as fd:
        index = json.load(fd)
    utilities.download_according_to_index(m, activity_net_index_walker(index))


if __name__ == '__main__':
    main()
