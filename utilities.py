# encoding: UTF-8

import sys
import os
import hashlib
import requests
import subprocess
import typing
from you_get.extractors import YouTube
from you_get.common import get_output_filename, tr
from you_get.util.strings import get_filename

if not (sys.version_info[0] is 3 and sys.version_info[1] >= 5):
    raise SystemError("Python < 3.5 not supported")


class Types:
    IndexWalker = typing.Generator[typing.Tuple[str, str], None, None]
    Path = str
    URL = str
    VideoID = str


def md5sum(whatever: typing.AnyStr) -> str:
    if isinstance(whatever, str) and os.path.isfile(whatever):
        with open(whatever, 'rb') as fd:
            whatever = fd.read()
    if not isinstance(whatever, bytes):
        raise TypeError()
    return hashlib.md5(whatever).hexdigest()


class PathMapper:
    def __init__(self, root: Types.Path):
        root = root.strip()
        root = os.path.expanduser(root)
        root = os.path.expandvars(root)
        root = os.path.abspath(root)
        os.makedirs(root, exist_ok=True)
        assert os.path.isdir(root)
        self.root = root

    def __getitem__(self, item: Types.Path) -> Types.Path:
        if item is None:
            return self.root
        item = item.strip()
        if item.startswith(os.path.sep) or (isinstance(os.path.altsep, str) and item.startswith(os.path.altsep)):
            return self.root + item
        else:
            return os.path.join(self.root, item)

    def resolve(self, *args: Types.Path) -> Types.Path:
        return os.path.join(self.root, *args)

    def r(self, *args: Types.Path) -> Types.Path:
        return self.resolve(*args)

    def exists(self, *args: Types.Path, md5: str = None) -> Types.Path:
        filename = self.resolve(*args)
        existence = os.path.exists(filename)
        if md5 is None:
            return existence
        else:
            return existence and md5sum(filename) == md5

    def ensure(self, *args: Types.Path) -> None:
        os.makedirs(self.resolve(*args), exist_ok=True)


def fetch(mapper: PathMapper = None, relpath: Types.Path = None, url: Types.URL = None, md5: str = None) -> None:
    mapper.ensure(os.path.dirname(relpath))
    if not mapper.exists(relpath, md5=md5):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                with open(mapper.resolve(relpath), 'wb') as fd:
                    fd.write(response.content)
        except:
            raise IOError('Cannot fetch {!r}'.format(url))
        else:
            if not mapper.exists(relpath, md5=md5):
                raise IOError('MD5 checksum miss match: {!r}'.format(relpath))


def is_video_file(target: Types.Path) -> bool:
    ffprobe = subprocess.run(['ffprobe', target], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return ffprobe.returncode is 0


def download_youtube_video(vid: Types.VideoID, target: Types.Path) -> None:
    if os.path.exists(target) and is_video_file(target):
        print('File exists and seems fine. skipping VID {!r}'.format(vid))
        return
    youtube = YouTube()
    output_dir = os.path.dirname(target)
    # since videos on YouTube may be stored as many fragments, merging is a must
    merge = True
    youtube.download_by_vid(vid, output_dir=output_dir, merge=merge)
    # a bite of dirty hack to find out where did you-get written the video to and rename it to its VideoID
    if 'id' in youtube.streams_sorted[0]:
        stream_id = youtube.streams_sorted[0]['id']
    else:
        stream_id = youtube.streams_sorted[0]['itag']
    ext = youtube.streams[stream_id]['container']
    url = YouTube.get_url_from_vid(vid)
    title = tr(get_filename(youtube.title))
    output_name = get_output_filename(url, title, ext, output_dir, merge)
    output_path = os.path.join(output_dir, output_name)
    os.rename(output_path, target)


def download_according_to_index(mapper: PathMapper, walker: Types.IndexWalker) -> None:
    subsets = set()
    jobs = []
    for vid, subset in walker:
        subsets.add(subset)
        jobs.append((vid, mapper.resolve(subset, vid)))
    for subset in subsets:
        mapper.ensure(subset)
    total = len(jobs)
    for i, (vid, target) in enumerate(jobs, start=1):
        print('Downloading {:6d}/{:6d} {!r} -> {}'.format(i, total, vid, target))
        try:
            download_youtube_video(vid, target)
        except KeyboardInterrupt:
            print('Interrupted. Exiting...')
            break
        except Exception as e:
            print('Failed during downloading {!r}: {}'.format(vid, e))


ffprobe_help = subprocess.run(['ffprobe', '-h'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
if ffprobe_help.returncode is not 0:
    raise SystemError('Cannot find \'{ffprobe}\'')
