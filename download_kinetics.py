# encoding: UTF-8

from zipfile import ZipFile
import json
import os
import utilities

indexes = {url.split('/')[-1]: (url, md5) for url, md5 in
           [('https://deepmind.com/documents/66/kinetics_train.zip', 'a74320139071cc8a6b29f1ec03e97fc9'),
            ('https://deepmind.com/documents/81/kinetics_test.zip', '2d2f1427069b52763441745934817f4e'),
            ('https://deepmind.com/documents/65/kinetics_val.zip', 'be2c819abeb5e65446e7acea0ad5f2ca')]}


def kinetices_index_walker(index: dict):
    for vid, info in index.items():
        subset = info['subset']
        yield vid, subset


def main():
    m = utilities.PathMapper('kinetics')
    for relpath, (url, md5) in indexes.items():
        utilities.fetch(mapper=m, relpath=relpath, url=url, md5=md5)
        zipfile = ZipFile(m[relpath])
        index = relpath.replace('zip', 'json')
        index = os.path.join(os.path.splitext(relpath)[0], index)
        index = zipfile.read(index)
        index = json.loads(index)
        utilities.download_according_to_index(m, kinetices_index_walker(index))


if __name__ == '__main__':
    main()
