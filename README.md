# Activity Net Dataset downloader

## Requirements

+ Python >= 3.5
+ requests >= 2.13
+ you-get == 0.4.803

## Usage

### Clone this repo
```shell
git clone https://gitlab.com/yufeiliu94/activity-net-downloader.git
cd activity-net-downloader
```

### Setup Virtual Environment (Optional)
```shell
virtual -p $(which python3) venv
source venv/bin/activate
```

### Install Requirements
```shell
pip install -r requirements.txt
```

### Downloading
```shell
# Well, here are 325, 119 video files to download...
python download_activity_net.py
python download_kinetics.py
```
